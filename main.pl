use LWP::Simple;
use LWP::UserAgent;
use HTTP::Request;

for($i=1;$i<=10000;$i++)
{
	if($i != 404)#try going to http://xkcd.com/404 - the laughter never stops till it screws up your code.
	{
		
		$target = &geturl($i);
		$ua = LWP::UserAgent->new;
		$response = $ua->get($target);
		if($response->code() == "404")
		{
			last;
		}
		@body = split('\n',$response->content);
		foreach my $i (@body)
		{
			if($i =~ m`(http://imgs\.xkcd\.com/comics/[a-zA-Z0-9_()]*\.(png|jpg|jpeg))`)
			{
				$filepath = $1;
				
				if($i =~ /([a-zA-Z0-9_()]*\.(png|jpg|jpeg))/)
				{
					$filename = $1;
					$ua->mirror($filepath, "C:\\xkcd\\$filename");
					print $filename . " Downloaded\n";
					last;
				}
			}
		}
	}
}

sub geturl
{
	my($num) = @_;
	return("http://xkcd.com/$num/");
}